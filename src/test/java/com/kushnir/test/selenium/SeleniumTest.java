package com.kushnir.test.selenium;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

public class SeleniumTest {

    private static WebDriver driver;

    private static JavascriptExecutor js;

    private final static String loginInput = "//*[@id=\"inputUsername\"]";
    private final static String passwordInput = "//*[@id=\"inputPassword\"]";
    private final static String signInButton = "/html/body/div/ui-view/login/form/button";
    private final static String isErrorTab = "//a[contains(., 'InError Registrations')]"; // "/html/body/div/ui-view/tabs/div/ul/li[1]/a";

    @BeforeClass
    public static void setUp() {
        System.setProperty("webdriver.gecko.driver", "/home/akushnir/Downloads/geckodriver");
        driver = new FirefoxDriver();
        js = (JavascriptExecutor) driver;

        driver.manage().window()
                .setSize(new Dimension(1200, 800));

        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
    }

    @Before
    public void before() {
        driver.get("http://localhost:8080/energy__registration--workbench-web");
    }

    @After
    public void after() {
        /*driver.quit();
        driver.close();*/
    }

    @Test
    public void logIn() {
        WebElement ElementLoginInput = getElement(loginInput);
        WebElement ElementPasswordInput = getElement(passwordInput);
        WebElement ElementSignInButton = getElement(signInButton);

        ElementLoginInput.sendKeys("rwbagent");
        ElementPasswordInput.sendKeys("654321");
        ElementSignInButton.click();

        WebElement ElementErrorRegistrationTab = getElement(isErrorTab);

        js.executeScript(String.format("arguments[0].style.background = '%s';", "orange"), ElementErrorRegistrationTab);
    }

    private WebElement getElement(final String xPath) {
        return (new WebDriverWait(driver, 5)).until(new ExpectedCondition<WebElement>() {
            public WebElement apply(WebDriver d) {
                return d.findElement(By.xpath(xPath));
            }
        });
    }

}
